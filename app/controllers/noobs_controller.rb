class NoobsController < ApplicationController
  before_action :set_noob, only: [:show, :edit, :update, :destroy, :follow, :unfollow, :im_bad]
  before_action :admin_page!, only: [:edit, :update, :destroy, :new, :create]
  before_action :set_followed, only: [:index, :follows]

  def follow_response
    respond_to do |format|
      format.js {render json: @noob, status: :ok}
      format.html {redirect_to noobs_url}
    end
  end

  def follow
    current_user.noobs.push @noob
    follow_response
  end

  def unfollow
    current_user.noobs.delete @noob
    follow_response
  end

  def im_bad_successfull_lock
    @noob.lock_for_user! current_user # generate / update the lockBad
    respond_to do |format|
      format.js {render json: @noob, status: :ok}
      format.html {redirect_to noobs_url}
    end
  end

  def im_bad_failed_lock
    respond_to do |format|
      if @noob.locked_for_user?(current_user)
        format.js {render json: @noob, status: :locked}
      else
        format.js {render json: @noob, status: :unprocessable_entity}
      end
      format.html {redirect_to noobs_url}
    end
  end

  def im_bad
    # successfully is true only if not locked in last 7 days and updated successfully
    successfully = !@noob.locked_for_user?(current_user) && @noob.update(bad: @noob.bad+1)
    if successfully
      im_bad_successfull_lock
    else
      im_bad_failed_lock
    end
  end

  # GET /noobs
  # GET /noobs.json
  def index
    @noob = Noob.new if current_user.admin?
    @noobs = Noob.order(:bad).reverse_order
    @noobs = search_name(@noobs) if params[:noob_search]
    @noobs = @noobs.paginate(page: params[:page])
  end

  def follows
    @noob = Noob.new if current_user.admin?
    @noobs = current_user.noobs.order(:bad).reverse_order
    @noobs = search_name(@noobs) if params[:noob_search]
    @noobs = @noobs.paginate(page: params[:page])
    render :index
  end

  # GET /noobs/1
  # GET /noobs/1.json
  def show
    @mark = Mark.new
    @mark.noob_id = @noob.id
  end

  # GET /noobs/new
  def new
    @noob = Noob.new
  end

  # GET /noobs/1/edit
  def edit
    @mark = Mark.new
    @mark.noob_id = @noob.id
  end

  # POST /noobs
  # POST /noobs.json
  def create
    @noob = Noob.new(noob_params)

    respond_to do |format|
      if @noob.save
        format.html { redirect_to @noob, notice: 'Noob was successfully created.' }
        format.json { render json: @noob, status: :created }
      else
        format.html { render :new }
        format.json { render json: @noob.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /noobs/1
  # PATCH/PUT /noobs/1.json
  def update
    respond_to do |format|
      if @noob.update(noob_params)
        format.html { redirect_to @noob, notice: 'Noob was successfully updated.' }
        format.json { render json: @noob, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @noob.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /noobs/1
  # DELETE /noobs/1.json
  def destroy
    @noob.destroy
    respond_to do |format|
      format.html { redirect_to noobs_url, notice: 'Noob was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_noob
      @noob = Noob.find(params[:id])
    end

    def set_followed
      @followed = current_user.noobs
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def noob_params
      params.require(:noob).permit(:name, :bad)
    end

    def search_name(noobs)
      # TODO : check if safe
      query = params[:noob_search].to_s.gsub(/[^[:alnum:]]/, '')
      noobs = noobs.where("noobs.name LIKE('%#{query}%')")
    end
end
