json.array!(@users) do |user|
  json.extract! user, :id, :email, :group, :sign_in_count, :last_sign_in_at, :last_sign_in_ip, :failed_attempts, :locked, :updated_at, :created_at
  json.url user_url(user, format: :json)
end
