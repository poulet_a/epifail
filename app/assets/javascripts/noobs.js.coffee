updateNoob = (id, data) ->
  noob = {noob: data}
  $.ajax(url: "/noobs/#{id}", method: 'PATCH', data: noob, dataType: 'json', async: false).done((e) -> $.noob = e)
  return $.noob

getNoob = (id) ->
  noob = $.ajax(url: "/noobs/#{id}", method: 'GET', dataType: 'json', async: false).done((e) -> $.noob = e)
  return $.noob

# follow or not, update the front (link + icon)
noobUpdateFollowLink = (id) ->
  obj = $("#noobFollowLink_#{id}")
  if obj
    if obj.attr('href').contains('unfollow')
      obj.attr('href', "/noobs/#{id}/follow")
      obj.html("<span class='glyphicon glyphicon-star-empty'></span>")
    else
      obj.attr('href', "/noobs/#{id}/unfollow")
      obj.html("<span class='glyphicon glyphicon-star'></span>")
  return

# TODO : shoudn't check in the database for update before creating the form ?
# change the text in an input
changeNameInNameEdit = (that) ->
  return if not that.classList.contains("noobName")
  that.innerHTML = "<input id='noobNameEdit_#{getid(that)}' class='noobNameEdit form-control input-sm' value='#{that.textContent.trim()}' type='text' title='only a-Z,-,_,0-9, size 2..10' placeholder='#{that.textContent}' pattern=([a-z\-_0-9]{2,10}(></input>"
  return

# updates the noob by changing his name, and then changes the input in a simple text
changeNameEditInName = (that) ->
  return if not that?
  return if not that.classList.contains('noobNameEdit')
  updateNoob(getid(that), {name: that.value})
  that.parentNode.innerHTML = $.noob.name
  return

# if the user vote for bad noob, it also update the span field with the number
initNoobImBad = () ->
  $(document).on('ajax:success', '.noobImBad', (event, data, status, xhr) ->
    $("#noobBad_#{data.id}").text(data.bad + ' ')
    return
  )
  return

# if the user is an admin, he should be allowed to edit the noob.
# so he can do it inline by clicking on it. click somewhere else to update the mo**a f**ka
initNoobNameEdit = () ->
  if isAdmin
    $(document).on('click', 'html', (e) ->
      t = e.target
      if t.classList.contains('noobNameEdit')
        return
      changeNameEditInName($('.noobNameEdit')[0])
      if t.classList.contains('noobName')
        changeNameInNameEdit(t)
      return
    )
  return

# the link to follow the noob should also update the line
initNoobFollowLinks = () ->
  $(document).on('ajax:success', '.noobFollowLink', (event, data, status, xhr) ->
    noobUpdateFollowLink(data.id)
    return
  )
  return

# the link to destroy a noob should destroy the line too
initNoobDestroyLinks = () ->
  $(document).on('ajax:success', '.noobDestroyLink', (event, data, status, xhr) ->
    $("#noobLine_#{getid(this)}").remove()
    return
  )
  return

# init the form to add a noob
initNoobForm = () ->
  $('#new_noob').bind('ajax:success', (event, data, status, xhr) ->
    $('#noob-list').append(noobNewLine(data))
    $("#noob_name").val('')
    # the tootip of a potential previous error HAS TO BE DELETED !
    if document.location.pathname == '/noobs/follows'
      $.post("/noobs/#{data.id}/follow").success().then(noobUpdateFollowLink(data.id))
    return
  )
  $('#new_noob').bind('ajax:error', (event, data, status, xhr) ->
    #$('#noob_name').tooltip({
    #  content: "Awesome title!"
    #})
    # add tool tip about the errror data = {field: reason}
  )
  return

# get the current id of the noob from a html element
getid = (that) ->
  return that.id.split('_')[1]

# this is not a security measure, just front end feature
isAdmin = () ->
  return $("#user-infos-group").text().indexOf("admin") != -1 # TODO : find the way to user User.groups.admin

noobNewLine = (json) ->
  base = "<tr id='noobLine_#{json.id}' class='noobLine'>"
  base += "<td id='noobId_#{json.id}' class='noobId'>##{json.id}</td>"
  base += "<td id='noobName_#{json.id}' class='noobName'>#{json.name}</td>"
  base += "<td><span id='noobBad_#{json.id}' class='noobBad'>0 </span><a rel='nofollow' id='noobImBad_#{json.id}' href='/noobs/#{json.id}/im_bad' data-type='json' data-remote='true' data-method='POST' class='noobImBad'><span class='glyphicon glyphicon-plus'></span></a></td>"
  base += "<td><a rel='nofollow' id='noobFollowLink_#{json.id}' href='/noobs/#{json.id}/follow' data-type='json' data-remote='true' data-method='POST' class='btn btn-default btn-sm noobFollowLink'><span class='glyphicon glyphicon-star-empty'></span></a></td>"
  base += "<td><a id='noobShowLink_#{json.id}' href='/noobs/#{json.id}' class='btn btn-default btn-sm noobShowLink'>Show</a></td>"

  admin = "<td><a id='noobEditLink_#{json.id}' href='/noobs/#{json.id}/edit' class='btn btn-default btn-sm noobEditLink'>Edit</a></td>"
  admin += "<td><a rel='nofollow' id='noobDestroyLink_#{json.id}' href='/noobs/#{json.id}' data-type='json' data-remote='true' data-method='delete' data-confirm='Are you sure?' class='btn btn-default btn-sm noobDestroyLink'>Destroy</a></td><td></td></tr>"
  return if isAdmin() then (base + admin) else (base)

$ ->
  initNoobImBad()
  #initNoobImBadAutoUpdate()
  initNoobNameEdit() if isAdmin()
  initNoobFollowLinks()
  initNoobDestroyLinks()
  initNoobForm() if isAdmin
  return
