User.create([
  {email: 'admin@epifail.fr', password: 'admin|quiet', confirmed_at: Time.now, group: 'admin'},
  {email: 'user1@epifail.fr', password: 'please123', confirmed_at: Time.now},
]) if User.count.zero?

Noob.create([
  {name: 'poulet_a'},
  {name: 'broggi_t'},
])

User.find(1).noobs << Noob.all[0]
User.find(1).noobs << Noob.all[1]
User.find(2).noobs << Noob.all[1]
